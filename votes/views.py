from django.http import JsonResponse, HttpResponse, Http404
from rest_framework import views

from votes.models import Candidate, Vote, Attribute, Comment


class CandidateList(views.APIView):
    def get(self, request, format=None):
        results = []
        major = request.GET.get('major', 'C').upper()

        for candidate in Candidate.objects.filter(major=major).all():
            result = {
                "name": candidate.full_name,
                "id": candidate.id,
                "already_voted": Vote.objects.filter(voter=request.user, candidate=candidate).exists() or Comment.objects.filter(voter=request.user, candidate=candidate).exists()
            }
            results.append(result)

        return JsonResponse({"result": results})


class CandidateDetail(views.APIView):
    def get_object(self, pk):
        try:
            return Candidate.objects.get(id=pk)
        except Candidate.DoesNotExist:
            raise Http404

    def get(self, request, candidate_id, format=None):
        results = []

        candidate = self.get_object(candidate_id)
        for vote in Vote.objects.filter(voter=request.user, candidate=candidate):
            results.append(vote.attribute.id)

        if Comment.objects.filter(voter=request.user, candidate=candidate).exists():
            comment = Comment.objects.get(voter=request.user, candidate=candidate).text
        else:
            comment = ''

        return JsonResponse({"votes": results, "comment": comment})


class AttrList(views.APIView):
    def get(self, request, format=None):
        results = []
        for attribue in Attribute.objects.all():
            result = {
                "name": attribue.name,
                "id": attribue.id,
            }
            results.append(result)

        return JsonResponse({"result": results})


class VoteView(views.APIView):
    def post(self, request, format=None):
        candidate_id = request.data['candidate_id']
        attr_id = request.data['attr_id']

        candidate = Candidate.objects.get(id=candidate_id)
        attr = Attribute.objects.get(id=attr_id)

        current_vote, created = Vote.objects.get_or_create(candidate=candidate, attribute=attr, voter=request.user)

        if not created:
            current_vote.delete()

        return HttpResponse()


class CommentView(views.APIView):
    def post(self, request, format=None):
        candidate_id = request.data['candidate_id']
        comment_text = request.data['text']

        candidate = Candidate.objects.get(id=candidate_id)

        current_comment, created = Comment.objects.get_or_create(candidate=candidate, voter=request.user)

        current_comment.text = comment_text
        current_comment.save()

        return HttpResponse()
