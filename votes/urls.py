from django.urls import path, include
from rest_framework.schemas import get_schema_view
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from votes.views import CandidateList, CandidateDetail, AttrList, \
    VoteView, CommentView

urlpatterns = [
    path('', get_schema_view()),
    path('auth/', include(
        'rest_framework.urls', namespace='rest_framework')),
    path('auth/token/obtain/', TokenObtainPairView.as_view()),
    path('auth/token/refresh/', TokenRefreshView.as_view()),

    path("candidates/<int:candidate_id>/", CandidateDetail.as_view()),
    path("candidates/", CandidateList.as_view()),
    path("attributes/", AttrList.as_view()),
    path("vote/", VoteView.as_view()),
    path("comment/", CommentView.as_view())
]
