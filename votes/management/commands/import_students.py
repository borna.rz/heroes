import csv
import random
import string

from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.core.management import BaseCommand
from django.template.loader import render_to_string

from votes.models import Candidate


def create_email_func(msg_plain, to, msg_html):
    def func():
        print("sending email to {}".format(to))

        send_mail(
            'خوب‌ها و خوب‌ترین‌های ۹۳',
            msg_plain,
            'bornaarzi@gmail.com',
            [to],
            html_message=msg_html,
        )

    return func


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('file_name', type=str)

    def handle(self, *args, **options):
        with open(options['file_name'], newline='') as file:
            reader = csv.reader(file)
            next(reader, None)

            email_tasks = []
            for row in reader:
                username = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))
                password = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))
                name = row[1]
                surname = row[2]
                email = row[3]
                major = row[6].upper()

                print("doing {} {}".format(name, surname))

                Candidate.objects.create(name=name, surname=surname, major=major)
                print("created candidate")

                User.objects.create_user(username=username, password=password)
                print("created user")

                msg_plain = render_to_string('../templates/email.txt',
                                             {'username': username, 'password': password, 'name': name})
                msg_html = render_to_string('../templates/email.html',
                                            {'username': username, 'password': password, 'name': name})

                email_tasks.append(create_email_func(msg_plain, email, msg_html))

            for task in email_tasks:
                task()

            print("Importing Is Done!")
