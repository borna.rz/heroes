from django.contrib.auth.models import User
from django.db import models


class Candidate(models.Model):
    name = models.CharField(max_length=30, null=False)
    surname = models.CharField(max_length=30, null=False)
    major = models.CharField(max_length=10, null=False)

    @property
    def full_name(self):
        return self.name + " " + self.surname


class Attribute(models.Model):
    name = models.CharField(max_length=20, null=False)


class Vote(models.Model):
    voter = models.ForeignKey(User, null=False, on_delete=models.CASCADE)
    candidate = models.ForeignKey(Candidate, null=False, on_delete=models.CASCADE)
    attribute = models.ForeignKey(Attribute, null=False, on_delete=models.CASCADE)


class Comment(models.Model):
    voter = models.ForeignKey(User, null=False, on_delete=models.CASCADE)
    candidate = models.ForeignKey(Candidate, null=False, on_delete=models.CASCADE)
    text = models.TextField()
