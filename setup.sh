#!/usr/bin/env bash

./wait-for-it.sh db:5432
python manage.py migrate --noinput
gunicorn heroes.wsgi --bind 0.0.0.0:8000
