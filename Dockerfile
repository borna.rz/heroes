FROM python:3.6
ENV PYTHONUNBUFFERED 1

RUN pip install coreapi django djangorestframework djangorestframework-simplejwt psycopg2 gunicorn django-cors-headers

ADD . /code
WORKDIR /code

RUN python manage.py collectstatic --noinput

